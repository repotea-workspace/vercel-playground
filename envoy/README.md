envoy
===



https://www.envoyproxy.io/docs/envoy/latest/api-v3/config/endpoint/v3/endpoint_components.proto#envoy-v3-api-msg-config-endpoint-v3-endpoint

> Attention:
> 
> The form of host address depends on the given cluster type. For STATIC or EDS, it is expected to be a direct IP
> address (or something resolvable by the specified resolver in the Address). For LOGICAL or STRICT DNS, it is expected to
> be hostname, and will be resolved via DNS.

