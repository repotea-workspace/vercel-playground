import type {VercelRequest, VercelResponse} from '@vercel/node';

import {createProxyMiddleware, RequestHandler} from 'http-proxy-middleware';

// const proxy: RequestHandler = createProxyMiddleware('/api/proxy/**', {
//   target: process.env.BACKEND_URL,
//   ws: true, // enable proxying WebSockets
//   pathRewrite: { "^/api/proxy": "" }, // remove `/api/proxy` prefix
// });


const proxy: RequestHandler = createProxyMiddleware(
  'wss://rpc.darwinia.network',
  {
    ws: true,
    changeOrigin: true
  });

export default async function (req: VercelRequest, res: VercelResponse) {
  // res.setHeader('content-type', 'application/json');
  //
  // res.statusCode = 200;
  //
  // const body = {
  //   hello: 'world',
  // };
  // res.end(JSON.stringify(body, null, 2));

  // createProxyMiddleware('/api/proxy/**', {
  //   target: 'wss://rpc.darwinia.network',
  //   ws: true, // enable proxying WebSockets
  //   pathRewrite: { "^/api/proxy": "" }, // remove `/api/proxy` prefix
  // });

  proxy.upgrade(req, res, (err) => {
    if (err) {
      throw err;
    }

    throw new Error(`Local proxy received bad request for ${req.url}`);
  });

}

export const config = {
  api: {
    // Proxy middleware will handle requests itself, so Next.js should
    // ignore that our handler doesn't directly return a response
    externalResolver: true,
    // Pass request bodies through unmodified so that the origin API server
    // receives them in the intended format
    bodyParser: false,
  },
}
